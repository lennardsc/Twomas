import sqlite3
import requests
import webbrowser
import tkinter as tk
import os
import re
from tkinter import simpledialog  # Import the simpledialog module
from tkinter import Frame, Text, Entry, Scrollbar, Button
from tkinter import Button
import uuid
import websockets
import asyncio
import threading
from datetime import datetime
from tkinter import messagebox
from PIL import Image, ImageTk
import tkinterdnd2 as tkdnd


from dotenv import load_dotenv  # Import load_dotenv
# Load environment variables from .env
load_dotenv()
access_token = None  # Add this global variable

# SQLite Database Initialization
def initialize_database():
    conn = sqlite3.connect('streamers.db')
    cursor = conn.cursor()

    # Create the streamers table if it doesn't exist
    cursor.execute('''
        CREATE TABLE IF NOT EXISTS streamers (
            id INTEGER PRIMARY KEY,
            name TEXT,
            twitch_id TEXT,
            is_live INTEGER
        )
    ''')
    # Create a new table to store user activity
    cursor.execute('''
        CREATE TABLE IF NOT EXISTS user_activity (
            id INTEGER PRIMARY KEY,
            username TEXT,
            streamer_name TEXT,
            start_time TIMESTAMP,
            end_time TIMESTAMP
        )
    ''')

    conn.commit()
    conn.close()
client_id = os.getenv('TWITCH_CLIENT_ID')

# Fetch and Display Live Streamers for the user
def get_and_display_live_streamers(user_id):
    if access_token is None or user_id is None:
        return []

    headers = {
        'Client-ID': client_id,  # Use the Client-ID from the .env file
        'Authorization': f'Bearer {access_token}'
    }

    response = requests.get(f'https://api.twitch.tv/helix/streams/followed?user_id={user_id}', headers=headers)
    data = response.json()

    print("Data received from Twitch API:", data)  # Add this line to debug
    # Update the SQLite database with live streamer information
    conn = sqlite3.connect('streamers.db')
    cursor = conn.cursor()

    cursor.execute('DELETE FROM streamers')

    for stream in data.get('data', []):
        cursor.execute('INSERT INTO streamers (name, twitch_id, is_live) VALUES (?, ?, ?)',
                    (stream['user_name'], stream['user_id'], 1))

    conn.commit()

    # Retrieve the list of live streamer names from the updated database
    streamer_names = [row[0] for row in cursor.execute('SELECT name FROM streamers WHERE is_live=1')]

    conn.close()

    return streamer_names

def create_twitch_embed(streamer_name):
    url = f"https://player.twitch.tv/?channel={streamer_name}&parent=twitch.tv&muted=false"
    
    # Open the URL in the default web browser
    webbrowser.open(url)

def select_streamer():
    selected_streamer = streamer_var.get()
    streamer_name = selected_streamer.lower()

    create_twitch_embed(streamer_name)
    record_user_activity(username, streamer_name, datetime.now(), None)

# Add a function to prompt for access token within the GUI
def prompt_access_token():
    global access_token
    access_token = simpledialog.askstring("Access Token", "Enter your Twitch access token:")

# Authentication with Twitch
def authenticate_with_twitch():
    client_id = os.getenv('TWITCH_CLIENT_ID')
    redirect_uri = 'http://localhost:3000'
    scope = 'channel%3Amanage%3Apolls+channel%3Aread%3Apolls+user:read:follows+chat:read+chat:edit+channel:manage:videos+user:read:broadcast'  # Add the required scopes
    state = str(uuid.uuid4())  # Generate a unique state string

    auth_url = f'https://id.twitch.tv/oauth2/authorize?response_type=token&client_id={client_id}&redirect_uri={redirect_uri}&scope={scope}&state={state}'

    # Open the Twitch authentication URL in an external browser
    webbrowser.open(auth_url)

    # Prompt the user to enter the access token within the GUI
    prompt_access_token()

# Get the user's own user ID
def get_user_id():
    if access_token is None:
        return None

    headers = {
        'Client-ID': client_id,  # Use the Client-ID from the .env file
        'Authorization': f'Bearer {access_token}'
    }

    user_url = 'https://api.twitch.tv/helix/users'

    response = requests.get(user_url, headers=headers)
    user_data = response.json()

    if 'data' in user_data and len(user_data['data']) > 0:
        return user_data['data'][0]['id']

    return None

def get_user_info():
    if access_token is None:
        return None

    headers = {
        'Client-ID': client_id,
        'Authorization': f'Bearer {access_token}'
    }

    user_url = 'https://api.twitch.tv/helix/users'

    response = requests.get(user_url, headers=headers)
    user_data = response.json()

    if 'data' in user_data and len(user_data['data']) > 0:
        return user_data['data'][0]['login']  # 'login' contains the username

    return None

# Function to record user activity in the database
def record_user_activity(username, streamer_name, start_time, end_time):
    conn = sqlite3.connect('streamers.db')
    cursor = conn.cursor()

    cursor.execute('INSERT INTO user_activity (username, streamer_name, start_time, end_time) VALUES (?, ?, ?, ?)',
                (username, streamer_name, start_time, end_time))

    conn.commit()
    conn.close()

import requests
import os
import subprocess
import sys

def check_for_updates(current_version):
    repo_url = "https://codeberg.org/lennardsc/Twomas"
    releases_url = f"{repo_url}/releases"
    
    try:
        response = requests.get(releases_url)
        response.raise_for_status()  # Raise an HTTPError for bad responses

        releases = response.json()
        if releases:
            latest_version = releases[0]['tag_name']  # Assumes releases are ordered by date
            if current_version < latest_version:
                return latest_version
            else:
                return None
        else:
            print("No releases found.")
            return None
    except requests.exceptions.RequestException as e:
        print(f"Error checking for updates: {e}")
        return None
    except Exception as e:
        print(f"Unexpected error: {e}")
        return None


def download_and_apply_update(repo_url):
    try:
        # Replace 'your_executable_name' with the actual name of your executable
        subprocess.run(["main.exe", "https://codeberg.org/lennardsc/Twomas/releases/latest/download/main.exe"], check=True)
    except subprocess.CalledProcessError as e:
        print(f"Error updating the application: {e}")
        return False
    return True

def restart_application():
    python_executable = sys.executable
    os.execl(python_executable, python_executable, *sys.argv)

# Assume your current version is stored as a string
current_version = "0.2"

latest_version = check_for_updates(current_version)
if latest_version:
    print(f"A new version ({latest_version}) is available. Updating...")

    if download_and_apply_update(repo_url):
        print("Update successful. Restarting the application...")
        restart_application()
    else:
        print("Update failed.")
else:
    print("You have the latest version.")


# Initialize the SQLite database
initialize_database()


# Create a Tkinter window
root = tk.Tk()
root.title("Twomas")


# Create a StringVar to store the selected streamer
streamer_var = tk.StringVar()

# Authenticate with Twitch and then fetch live streamers for the user
authenticate_with_twitch()
user_id = get_user_id()
username = get_user_info()
streamer_names = get_and_display_live_streamers(user_id)

# Create buttons or a dropdown to select a live streamer
if streamer_names:
    streamer_dropdown = tk.OptionMenu(root, streamer_var, *streamer_names)
    streamer_dropdown.pack()

select_button = tk.Button(root, text="Select Streamer", command=select_streamer)
select_button.pack()

# Create a new window for the chat
def open_chat_window():
    chat_window = tk.Toplevel(root)
    chat_window.title("Chat")

    # Get the full path to the image file
    image_path = os.path.abspath("background_chat.jpg")

    try:
        # Open the image with Pillow
        pil_image = Image.open(image_path)
        # Convert the Pillow image to a Tkinter PhotoImage
        tk_image = ImageTk.PhotoImage(pil_image)
    except Exception as e:
        print(f"Error loading image: {e}")
        return

    # Create a Canvas widget to place the background image
    canvas = tk.Canvas(chat_window, width=pil_image.width, height=pil_image.height)
    canvas.pack()

    # Keep a reference to the background image to prevent garbage collection
    canvas.background_image = tk_image

    # Place the background image on the canvas
    canvas.create_image(0, 0, anchor=tk.NW, image=tk_image)

    chat_display = create_chat_display(chat_window)
    start_chat_thread(streamer_var.get(), chat_display)

    # Create an input field for chat messages in the chat window
    message_entry = Entry(chat_window)
    message_entry.pack()

    # Create a "Send" button in the chat window
    send_button = Button(chat_window, text="Send", command=lambda: send_message(message_entry.get(), chat_display))
    send_button.pack()

    # Set the background of the Text widget to be transparent
    chat_display.config(highlightthickness=0, insertbackground='white', insertofftime=0)

chat_button = tk.Button(root, text="Open Chat Window", command=open_chat_window)
chat_button.pack()


def start_chat_thread(streamer_name, chat_display):
    global chat_thread, stop_chat_thread
    stop_chat_thread = False
    chat_thread = threading.Thread(target=chat_thread_function, args=(streamer_name, chat_display))
    chat_thread.start()

# Function to stop the chat thread
def stop_chat_thread():
    global stop_chat_thread
    stop_chat_thread = True
    record_user_activity(username, streamer_name, start_time, datetime.now())


# Function to stop the chat thread
def chat_thread_function(streamer_name, chat_display):
    loop = asyncio.new_event_loop()
    asyncio.set_event_loop(loop)

    uri = "wss://irc-ws.chat.twitch.tv:443"
    
    async def chat():
        global stop_chat_thread

        while not stop_chat_thread:
            async with websockets.connect(uri) as websocket:
                # Authenticate
                await websocket.send(f"PASS oauth:{access_token}")
                await websocket.send(f"NICK {username}")
                await websocket.send(f"JOIN #{streamer_name}")

                while not stop_chat_thread:
                    message = await websocket.recv()
                    formatted_message = format_chat_message(message)

                    # Update the chat display in the chat window
                    root.after(0, lambda: chat_display.insert(tk.END, f"{formatted_message}\n"))
                    root.after(0, lambda: chat_display.yview(tk.END))  # Auto-scroll to the bottom

    loop.create_task(chat())

    try:
        loop.run_forever()
    except KeyboardInterrupt:
        pass
    finally:
        loop.stop()
        loop.run_until_complete(loop.shutdown_asyncgens())
        loop.close()



def create_chat_display(parent):
    chat_display = Text(parent, wrap=tk.WORD)
    chat_display.pack()
    return chat_display

message_pattern = re.compile(r':(.+?)![^ ]+ PRIVMSG #\w+ :(.+)')

def format_chat_message(message):
    match = message_pattern.match(message)
    if match:
        username, text = match.group(1), match.group(2)
        return f'{username}: {text}'
    else:
        return message

def get_license_text():
    license_url = "https://codeberg.org/lennardsc/Twomas/raw/commit/1edf885d8a406d42daab48987aecc9ad997fbef8/LICENSE"  # Replace with the actual URL
    response = requests.get(license_url)
    
    if response.status_code == 200:
        return response.text
    else:
        return "License text not found."

def display_license():
    license_text = get_license_text()
    
    if license_text:
        license_window = tk.Toplevel(root)
        license_window.title("License")
        license_text_widget = tk.Text(license_window, wrap=tk.WORD)
        license_text_widget.pack()

        license_text_widget.insert(tk.END, license_text)  # Insert the license text into the Text widget
        license_text_widget.config(state=tk.DISABLED)  # Make the Text widget read-only

# Function to send a message to the chat
async def send_message(message, hat_display):
    if message and access_token is not None:
        streamer_name = streamer_var.get()
        uri = "wss://irc-ws.chat.twitch.tv:443"
        async with websockets.connect(uri) as websocket:
            await websocket.send(f"PASS oauth:{access_token}")
            await websocket.send(f"NICK {username}")
            await websocket.send(f"JOIN #{streamer_name}")
            await websocket.send(f"PRIVMSG #{streamer_name} :{message}")


# Create a button to display the license
license_button = tk.Button(root, text="View License", command=display_license)
license_button.pack()

# Function to open Codeberg Issues page in the default web browser
def open_codeberg_issues():
    codeberg_issues_url = "https://codeberg.org/lennardsc/Twomas/issues"
    webbrowser.open(codeberg_issues_url)

# Create a button to open Codeberg Issues page
issues_button = tk.Button(root, text="Report an Issue", command=open_codeberg_issues)
issues_button.pack()

# Global variable to control the chat thread
stop_chat_thread = False

# Function to handle issue reporting
def report_issue():
    # Get additional details from the user
    issue_title = simpledialog.askstring("Report an Issue", "Enter the issue title:")
    issue_description = simpledialog.askstring("Report an Issue", "Enter a detailed description of the issue:")

    # Construct the issue body with user input
    issue_body = f"**Title:** {issue_title}\n\n**Description:** {issue_description}"

    # Open Codeberg Issues page with pre-filled title and body
    codeberg_new_issue_url = f"https://codeberg.org/lennardsc/Twomas/issues/new?title={issue_title}&body={issue_body}"
    webbrowser.open(codeberg_new_issue_url)

# Create a button to allow users to report a new issue
report_issue_button = tk.Button(root, text="Report a New Issue", command=report_issue)
report_issue_button.pack()

# Start the Tkinter main loop
root.mainloop()
